// @ts-check
const { test, expect } = require('@playwright/test');

test('Test 1: has title', async ({ page }) => {
  await page.goto(global.BASE_URL);

  console.log(`----\nBASE_URL: ${global.BASE_URL}`);
  console.log(`----\nTEST_STR: ${global.TEST_STR}`);
  console.log(`----\nTEST_SPEC_CHAR: ${global.TEST_SPEC_CHAR}`);

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('Test 2: get started link', async ({ page }) => {
  await page.goto(global.BASE_URL);

  console.log(`----\nBASE_URL: ${global.BASE_URL}`);
  console.log(`----\nTEST_STR: ${global.TEST_STR}`);
  console.log(`----\nTEST_SPEC_CHAR: ${global.TEST_SPEC_CHAR}`);

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects page to have a heading with the name of Installation.
  await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
});
